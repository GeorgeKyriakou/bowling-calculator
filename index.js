/**
* @author: Georgios Kyriakou
* @description: Bowling microservice, that handles single games, and adds/returns/clears history of games.
**/

const express = require('express'); // https://expressjs.com/
const mongoose = require('mongoose'); // http://mongoosejs.com/
const bodyParser = require('body-parser'); // https://www.npmjs.com/package/body-parser
const cors = require('cors'); // https://www.npmjs.com/package/cors
const path = require('path');
const http = require('http');
const config = require('./config/db/mongo');
const app = express();
const port = 5000;

// declare routes
const GAME = require("./routes/game.js");
const HISTORY = require("./routes/history.js")

// db
mongoose.connect(config.database)
mongoose.connection.on('connected', ()=>{
  console.log('Connected to database '+ config.database)
});
mongoose.connection.on('error', (err)=>{
  console.error(err);
});

// middleware
app.use(cors());
app.use(bodyParser.json());

// use routes
app.use('/game', GAME);
app.use('/history', HISTORY);


app.set('port', port)
const server = app.listen(app.get('port'),()=>{
    console.log('Listening on', app.get('port'));
});

module.exports = server;
