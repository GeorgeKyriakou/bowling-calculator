const chai = require('chai');
const expect = require('chai').expect;

chai.use(require('chai-http'));

const server = require('../index.js');

describe('API endpoint /game/', () => {
  describe('Test outcomes (also tests /reset endpoint)',() => {
    it('Throw: should return new frame score', (done) => {
      return chai.request(server)
      .get('/game/reset')
      .then(()=>{
        chai.request(server)
        .post('/game/frame')
        .send({
          first: 1,
          second: 2,
          third: 0
        })
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(typeof res.body).to.be.an('string');
          expect(res.body['score']).to.equal(3);
          done();
        });
      });
    });
    it('Strike: should add (10+first+second) +first +second', (done) => {
      return chai.request(server)
      .get('/game/reset')
      .then(()=>{
        chai.request(server)
        .post('/game/frame')
        .send({
          first: 10,
          second: 0,
          third: 0
        })
        .then((res) => {
          chai.request(server).post('/game/frame')
          .send({
            first: 1,
            second: 1,
            third: 0
          })
          .then((res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            expect(typeof res.body).to.be.an('string');
            expect(res.body['score']).to.equal(14)
            done();
          })
        });
      });
    });
    it('Spare: should add (10+first) +first +second', (done) => {
      return chai.request(server)
      .get('/game/reset')
      .then(()=>{
        chai.request(server)
        .post('/game/frame')
        .send({
          first: 5,
          second: 5,
          third: 0
        })
        .then((res) => {
          chai.request(server).post('/game/frame')
          .send({
            first: 1,
            second: 1,
            third: 0
          })
          .then((res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            expect(typeof res.body).to.be.an('string');
            expect(res.body['score']).to.equal(13);
            done();
          })
        });
      })
    });
  });

  describe('Test for invalid path',() => {
    it('should return Not Found', (done) => {
      return chai.request(server)
      .get('/game/INVALID_PATH')
      .then(function(res) {
        throw new Error('Path exists!');
      })
      .catch(function(err) {
        expect(err).to.have.status(404);
        done();
      });
    });
  });
});