const chai = require('chai');
const expect = require('chai').expect;

chai.use(require('chai-http'));

const server = require('../index.js'); // Our server

describe('API endpoint /history', () => {
  it('should return history', (done) => {
    return chai.request(server)
      .get('/history/load')
      .then(function(res) {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('array');
        done();
      });
  });

  it('should return empty array', (done) => {
    return chai.request(server)
      .get('/history/clear')
      .then(function(res) {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('array');
        expect(res.body.length).to.equal(0);
        done();
      });
  });
  it('Add game to history', (done) => {
    return chai.request(server)
    .post('/history/add')
    .send({
          data: [
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 }
          ],
        totalScore: 30
      })
    .then(()=>{
      chai.request(server)
      .get('/history/load')
      .then((res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('array');
        expect(res.body.length).to.equal(1)
        expect(res.body[0]['data'].length).to.equal(10)
        expect(res.body[0]['totalScore']).to.equal(30)
        done();
      });
    });
  });
});