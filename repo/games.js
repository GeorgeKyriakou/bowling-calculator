const mongoose = require('mongoose');

const FrameSchema = new mongoose.Schema({
  first: {
    type: Number
  },
  second: {
    type: Number
  },
  third: {
    type: Number
  },
  total: {
    type: Number
  }
})


const GameSchema = new mongoose.Schema({
  data: {
    type: [FrameSchema]
  },
  totalScore: {
    type: Number
  }
});


const Games = module.exports = mongoose.model('games', GameSchema, 'games');

module.exports.addGame = function(game, cb){
  const newGame = new Games(game);
  newGame.save(cb);
}

module.exports.getHistory = function(cb){
    Games.find({}, cb);
}

module.exports.clearHistory = function(cb){
  mongoose.connection.collections['games'].drop(
    (err) => { cb() }
  );
}