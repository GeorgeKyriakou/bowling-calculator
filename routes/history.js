const express = require('express');
const router = express.Router();
const Games = require('../repo/games');

router.get('/load', (req, res, next)=>{
  Games.getHistory((err,result)=>{
    if (err) {
      res.json({ error: true })
    }
    res.json(result);
  })
})

router.post('/add', (req, res, next)=>{
  const newGame = req.body
  Games.addGame(newGame, (err,result)=>{
    if (err) {
      res.json({ error: true })
    }
    res.json(result);
  })
})

router.get('/clear', (req, res, next)=>{
  Games.clearHistory((err,result)=>{
    if (err) {
      res.json({ error: true })
    }
    if (result === undefined) {
      result = []
    }
    res.json(result);
  })
})
module.exports = router
