const express = require('express');
const router = express.Router();
const Games = require('../repo/games');
let strikeStack = [];
let strikeFlag = false;
let spareStack = [];
let spareFlag = false;
let frameStack = [];
let currentScore = 0;
let strikeScore = 0;
let spareScore = 0;

router.get('/reset', (req, res, next)=>{
  strikeStack = [];
  strikeFlag = false;
  spareStack = [];
  spareFlag = false;
  frameStack = [];
  currentScore = 0;
  strikeScore = 0;
  spareScore = 0;
  res.json({"score": currentScore})
})

router.post('/frame', (req, res, next)=>{
  const first = parseInt(req.body.first);
  const second = parseInt(req.body.second);
  const third = parseInt(req.body.third) || 0;
  const frameScore = first + second + third;

  let outcome;
  if (first === 10 || second === 10){
    outcome = "strike";
  } else if ((first + second) === 10) {
    outcome = "spare";
  } else {
    outcome = "throw";
  }

  try {
    switch ( outcome ) {
      case "strike": {
        lookIDidAStrike();
        if(frameStack.length === 9){
          currentScore = calculateFrame(first, second, third, frameScore)
        }
        break;
      }
      case "spare": {
        lookIDidASpare();
        if(frameStack.length === 9){
          currentScore = calculateFrame(first, second, 0, frameScore)
        }
        break;
      }
      default:  {
        currentScore = calculateFrame(first, second, third, frameScore);
        break;
      }
    }
    frameStack.push({
      "outcome": outcome,
      "first": first,
      "second": second,
      "third": third,
      "frameScore": frameScore,
      "currentScore": currentScore
    });
    res.send({"score": currentScore});
  } catch (e) {
    throw e;
  }
});

function calculateFrame(first, second, third, frameScore){
  strikeScore = strikeFlag? addStrikeScore(first, second , third) : 0;
  spareScore = spareFlag? addSpareScore(first, third) : 0;
  currentScore += strikeScore + spareScore + frameScore;
  strikeFlag = false;
  spareFlag = false;
  strikeScore = 0;
  spareScore = 0;
  return currentScore;
}
function addStrikeScore(first, second, third = 0) {
  strikeScore = strikeStack.length * 10 + first + second
  if (third) {
    strikeScore += third;
  }
  strikeStack = [];
  return strikeScore;
}

function addSpareScore(first) {
  spareScore = spareStack.length * 10 + first;
  spareStack = [];
  return spareScore;
}

function lookIDidAStrike() {
  strikeStack.push("St");
  strikeFlag = true;
}
function lookIDidASpare() {
  spareStack.push("Sp");
  spareFlag = true;
}
module.exports = router
